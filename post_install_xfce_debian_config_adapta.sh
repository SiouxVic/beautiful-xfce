#!/bin/bash

CUR_DIR=$(pwd)

#Adding the adapta stable repository
sudo add-apt-repository ppa:tista/adapta -y

#system update
sudo apt update && sudo apt upgrade -y

#Installing cursor,windows and icons themes
sudo apt install adapta-gtk-theme breeze-cursor-theme papirus-icon-theme -y

#Desktop theme
xfconf-query -c xsettings -p /Net/ThemeName -s Adapta

#Cursor theme
xfconf-query -c xsettings -p /Gtk/CursorThemeName -s xcursor-breeze

#Windows manager theme
xfconf-query -c xfwm4 -p /general/theme -s Adapta

#Installing icon theme
xfconf-query -c xsettings -p /Net/IconThemeName -s Papirus-Adapta

#Configuration keyboard shortcuts

#windows/logo keys trigger the application menu 
xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom/Super_L -n -t string -s xfce4-popup-whiskermenu
xfconf-query -c xfce4-keyboard-shortcuts -p /commands/custom/Super_R -n -t string -s xfce4-popup-whiskermenu

#drop-down terminal
xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Primary><Alt>t'  -n -t string -s xfce4-terminal --drop-down

cd $CUR_DIR
